import torch 
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from sklearn.model_selection import train_test_split
from tqdm.auto import tqdm
from utils import * 
from models import *
import argparse, os

parser = argparse.ArgumentParser()

parser.add_argument("-order", type=int, default=0, help="Order in the result file")
parser.add_argument("-MC", type=int, default=100, help='Number of Monte Carlo iteration')
parser.add_argument("-B", type=int, default=512, help="Batch size - integer value")
parser.add_argument("-N", type=int, default=20000, help="Dataset size - integer value")
parser.add_argument("-T", type=int, default=200, help="Sequence Length - integer value")
parser.add_argument("-D", type=int, default=3, help="Number of sources - integer value")
parser.add_argument("-M", type=int, default=8, help="Number of sensors - integer value")
parser.add_argument("-SNR", type=int, default=0, help="Signal to Noise ratio - real value")
parser.add_argument("-coherent", type=str2bool, default=False, help="Coherence of signal - boolean value")
parser.add_argument("-epoch", type=int, default=200, help="Number of epochs - integer value")
parser.add_argument("-lr", type=float, default=1e-3, help="learning rate - real value")
parser.add_argument("-wd", type=float, default=1e-9, help="weight decay - real value")
parser.add_argument("-perturbation", type=float, default=0.0, help="Perturbation of array")
parser.add_argument("-calibrated", type=str2bool, default=True, help="Is the array calibrated")

args = parser.parse_args()

ord = args.order
MC = args.MC
B = args.B
N = args.N
T = args.T
D = args.D
M = args.M
SNR = args.SNR

perturbation = args.perturbation
coherent = args.coherent
calibrated = args.calibrated

nbEpochs = args.epoch
lr = args.lr
wd = args.wd

name = 'saved_models/N={}_T={}_D={}_M={}_SNR={}'.format(N, T, D, M, SNR)
if not os.path.exists(name):
    os.mkdir(name)


Acc_0_list, Acc_1_list, Acc_2_list, Acc_3_list, Acc_4_list = [], [], [], [], []


# Monte Carlo iteration #

for _ in tqdm(range(MC)):


    #####################################
    ########## Data Generation ##########
    #####################################


    array_real = ArrayModel(M, perturbation=perturbation)

    noise = CorrelatedNoise(M)

    observations, angles = generate_data(N, T, D, SNR, array_real, noise, coherent)

    x_train, x_valid, y_train, y_valid = train_test_split(observations, angles, test_size=0.2)
    x_train, x_test, y_train, y_test = train_test_split(x_train, y_train, test_size=0.2)

    train_set = MUSIC_DATA(x_train, y_train)
    valid_set = MUSIC_DATA(x_valid, y_valid)
    test_set = MUSIC_DATA(x_test, y_test)

    train_loader = DataLoader(train_set, batch_size=B, shuffle=True)
    valid_loader = DataLoader(valid_set, batch_size=B, shuffle=False)
    test_loader = DataLoader(test_set, batch_size=B, shuffle=False)

    if calibrated:
        
        array = array_real

    else:

        array = ArrayModel(M, perturbation=0.0)

    ##############################
    ########## DA-MUSIC ##########
    ##############################


    model_0 = DA_MUSIC(M, D, array)
    optim_0 = optim.Adam(model_0.parameters(), lr=lr, weight_decay=wd)

    Loss_0, Val_0 = [], []

    bestVal0 = 1000

    for i in range(nbEpochs):
        # Train
        running_loss = 0.0
        for data in train_loader:
            input, target = data[0].to(dev), data[1].to(dev)
            optim_0.zero_grad()
            pred = model_0(input)
            loss = RMSPE(pred, target)
            loss.backward()
            optim_0.step()
            running_loss += loss.item()
        Loss_0.append(running_loss/len(train_loader))

        # Validation 
        with torch.no_grad():
            running_loss = 0.0
            for data in valid_loader:
                input, target = data[0].to(dev), data[1].to(dev)
                pred = model_0(input)
                loss = RMSPE(pred, target)
                running_loss += loss.item()
            Val_0.append(running_loss/len(test_loader))

            if Val_0[i] < bestVal0:
                bestVal0 = Val_0[i]
                torch.save(model_0.state_dict(), name+'/model_0_opt.pt')

    # Test
    model_0.load_state_dict(torch.load(name+'/model_0_opt.pt'))
    with torch.no_grad():
        running_loss = 0.0
        for data in test_loader:
            input, target = data[0].to(dev), data[1].to(dev)
            pred = model_0(input)
            loss = RMSPE(pred, target)
            running_loss += loss.item()
    Acc_0 = running_loss/len(test_loader)

    Acc_0_list.append(Acc_0)


    #######################################
    ########## Modified DA-MUSIC ##########
    #######################################


    model_1 = DA_MUSIC_v2(M, D, array, noise)
    optim_1 = optim.Adam(model_1.parameters(), lr=lr, weight_decay=wd)

    Loss_1, Val_1 = [], []

    bestVal1 = 1000

    for i in range(nbEpochs):

        # Train
        running_loss = 0.0
        for data in train_loader:
            input, target = data[0].to(dev), data[1].to(dev)
            optim_1.zero_grad()
            pred = model_1(input)
            loss = RMSPE(pred, target)
            loss.backward()
            optim_1.step()
            running_loss += loss.item()
        Loss_1.append(running_loss/len(train_loader))

        # Validation 
        with torch.no_grad():
            running_loss = 0.0
            for data in valid_loader:
                input, target = data[0].to(dev), data[1].to(dev)
                pred = model_1(input)
                loss = RMSPE(pred, target)
                running_loss += loss.item()
            Val_1.append(running_loss/len(test_loader))

            if Val_1[i] < bestVal1:
                bestVal1 = Val_1[i]
                torch.save(model_1.state_dict(), name+'/model_1_opt.pt')

    # Test
    with torch.no_grad():
        model_1.load_state_dict(torch.load(name+'/model_1_opt.pt'))
        running_loss = 0.0
        for data in test_loader:
            input, target = data[0].to(dev), data[1].to(dev)
            pred = model_1(input)
            loss = RMSPE(pred, target)
            running_loss += loss.item()
    Acc_1 = running_loss/len(test_loader)

    Acc_1_list.append(Acc_1)


    #########################
    ########## RNN ##########
    #########################


    model_2 = RNN(M, D)
    optim_2 = optim.Adam(model_2.parameters(), lr=lr, weight_decay=wd)

    Loss_2, Val_2 = [], []

    bestVal2 = 1000

    for i in range(nbEpochs):

        # Train
        running_loss = 0.0
        for data in train_loader:
            input, target = data[0].to(dev), data[1].to(dev)
            optim_2.zero_grad()
            pred = model_2(input)
            loss = RMSPE(pred, target)
            loss.backward()
            optim_2.step()
            running_loss += loss.item()
        Loss_2.append(running_loss/len(train_loader))

        # Validation 
        with torch.no_grad():
            running_loss = 0.0
            for data in valid_loader:
                input, target = data[0].to(dev), data[1].to(dev)
                pred = model_2(input)
                loss = RMSPE(pred, target)
                running_loss += loss.item()
            Val_2.append(running_loss/len(test_loader))

            if Val_2[i] < bestVal2:
                bestVal2 = Val_2[i]
                torch.save(model_2.state_dict(), name+'/model_2_opt.pt')

    # Test
    with torch.no_grad():
        model_2.load_state_dict(torch.load(name+'/model_2_opt.pt'))
        running_loss = 0.0
        for data in test_loader:
            input, target = data[0].to(dev), data[1].to(dev)
            pred = model_2(input)
            loss = RMSPE(pred, target)
            running_loss += loss.item()
    Acc_2 = running_loss/len(test_loader)

    Acc_2_list.append(Acc_2)


    #########################
    ########## CNN ##########
    #########################


    model_3 = CNN(M, D)
    optim_3 = optim.Adam(model_3.parameters(), lr=lr, weight_decay=wd)

    Loss_3, Val_3 = [], []

    bestVal3 = 1000

    for i in range(nbEpochs):
        # Train
        running_loss = 0.0
        for data in train_loader:
            input, target = data[0].to(dev), data[1].to(dev)
            optim_3.zero_grad()
            pred = model_3(input)
            loss = nn.BCELoss()(pred, multihot_encode(target))
            loss.backward()
            optim_3.step()
            running_loss += loss.item()
        Loss_3.append(running_loss/len(train_loader))

        # Validation 
        with torch.no_grad():
            running_loss = 0.0
            for data in valid_loader:
                input, target = data[0].to(dev), data[1].to(dev)
                pred = model_3(input)
                loss = RMSPE(multihot_decode(pred, model_3.D), target)
                running_loss += loss.item()
            Val_3.append(running_loss/len(test_loader))

            if Val_3[i] < bestVal3:
                bestVal3 = Val_3[i]
                torch.save(model_3.state_dict(), name+'/model_3_opt.pt')

    # Test
    model_3.load_state_dict(torch.load(name+'/model_3_opt.pt'))
    with torch.no_grad():
        running_loss = 0.0
        for data in test_loader:
            input, target = data[0].to(dev), data[1].to(dev)
            pred = model_3(input)
            loss = RMSPE(multihot_decode(pred, model_3.D), target)
            running_loss += loss.item()
    Acc_3 = running_loss/len(test_loader)

    Acc_3_list.append(Acc_3)


    ###########################
    ########## MUSIC ##########
    ###########################


    music_result = []
    array.array = array.array.to('cpu')
    for i in range(len(x_test)):
        music_result.append(MUSIC(x_test[i].T, D, array=array, noise=None)[1])
    music_result = torch.stack(music_result, dim=0).to(device=dev)
    Acc_4 = RMSPE(music_result, y_test).item()

    Acc_4_list.append(Acc_4)


# End of Monte Carlo iteration 

Acc_0_avg = sum(Acc_0_list) / MC
Acc_1_avg = sum(Acc_1_list) / MC
Acc_2_avg = sum(Acc_2_list) / MC
Acc_3_avg = sum(Acc_3_list) / MC
Acc_4_avg = sum(Acc_4_list) / MC


###########################################
########## Write results to file ##########
###########################################

content = ['Results for T={}, D={}, M={}, SNR={} \n'.format(T, D, M, SNR),
           'Accuracy of MUSIC: {} \n'.format(Acc_4_avg),
           'Accuracy of DA-MUSIC: {} \n'.format(Acc_0_avg),
           'Accuracy of proposed modified DA-MUSIC: {} \n'.format(Acc_1_avg),
           'Accuracy of proposed DD model: {} \n'.format(Acc_2_avg),
           'Accuracy of CNN model: {} \n'.format(Acc_3_avg)]

file = open('results.txt', 'r')
file_lines = file.readlines()
file.close()

for i in range(len(content)):
    file_lines[10 * ord + i] = content[i]

file = open('results.txt', 'w')
file.writelines(file_lines)
file.close()
