"""
generate an empty text file of 100 lines
"""

f = open('results.txt', 'w')
lines = ['\n' for _ in range(100)]
f.writelines(lines)
f.close()