import torch , argparse
from torch.utils.data import Dataset
from math import sqrt, factorial
from scipy.signal import find_peaks

dev = torch.device("cuda" if torch.cuda.is_available() else "cpu")

nbSamples_spectrum = 360
angles_spectrum = torch.arange(0, nbSamples_spectrum, 1) * torch.pi / nbSamples_spectrum - torch.pi / 2

nbSamples_cnn = 180


class MUSIC_DATA(Dataset):

    def __init__(self, observations, angles):
        self.observations = observations
        self.angles = angles

    def __getitem__(self, idx):
        return self.observations[idx], self.angles[idx]
    
    def __len__(self):
        return len(self.observations)


class ArrayModel:

    def __init__(self, M, perturbation=0.0):

        """
        INPUT:
        M: number of sensors
        perturbation: percent perturbation added to the sensor positions
        """

        self.M = M
        self.array = torch.arange(0, M, 1) + (torch.rand(M) * 2 - 1) * perturbation
    
    def get_steering_vector(self, theta):

        """
        get the steering vector with respect to angle
        
        INPUT:
        theta: incident angle of size (D) or (B, D)
        
        OUPUT:
        steering vector of size (M, D) or (B, M, D)
        """

        if len(theta.shape)==1:
            return torch.exp(- 1j * self.array.reshape(-1, 1) * torch.pi * torch.sin(theta).reshape(1, -1))
        
        elif len(theta.shape)==2:
            return torch.exp(- 1j * self.array.reshape(1, -1, 1) * torch.pi * torch.sin(theta).unsqueeze(1))
        

class CorrelatedNoise:

    def __init__(self, M, alpha=0.0, sigma_epsilon=0.1):

        """
        INPUT:
        M: number of sensors
        alpha: coefficient of AR(1) noise
        """

        self.M = M
        self.alpha = alpha
        self.sigma_epsilon = sigma_epsilon
        self.sigma_noise = sigma_epsilon / sqrt(1 - abs(alpha) ** 2)
        
        self.cov = torch.zeros(M, M, dtype=torch.complex64)

        for i in range(M):
            for j in range(M):
                if i == j:
                    self.cov[i, j] = 1
                elif i > j:
                    self.cov[i, j] = alpha ** (i - j)
                else:
                    self.cov[i, j] = alpha.conjugate() ** (j - i)

        self.cov = self.cov * self.sigma_noise ** 2
        self.cov_inv = torch.linalg.inv(self.cov)
        self.L = torch.linalg.cholesky(self.cov_inv)
        self.U = self.L.T.conj()

    def generate(self, *args):
        
        noise = torch.zeros(*args, self.M, dtype=torch.complex64)
        noise[..., 0] = (torch.randn(args) + 1j * torch.randn(args)) * self.sigma_noise / sqrt(2)
        
        for i in range(1, self.M):
            noise[..., i] = self.alpha * noise[..., i - 1] + (torch.randn(args) + 1j * torch.randn(args)) * self.sigma_epsilon / sqrt(2)
        
        return noise
    

def generate_direction(D, gap=0.1):

    """
    generate D random directions

    INPUT:
    D: number of sources
    gap: min difference between directions

    OUTPUT:
    theta: a vector of size (D)
    """

    while True:

        theta = torch.rand(D) * torch.pi - torch.pi / 2
        theta = theta.sort()[0]
        
        if torch.min(theta[1:] - theta[:-1]) > gap and theta[-1] - theta[0] < torch.pi - gap:
            break 
    
    return theta
    

def generate_signal(T, D, SNR, array: ArrayModel, noise: CorrelatedNoise, coherent=False):

    """
    generate an array signal
    
    INPUT:
    T: length of signals
    D: number of sources
    SNR: signal to noise ratio
    array: predefined array model
    noise: predefined noise model
    coherent: coherence of signals
    
    OUTPUT:
    signal of size (M, T) and corresponding angle of size (D)
    """
    
    theta = generate_direction(D)
    A = array.get_steering_vector(theta)
    
    if not coherent:
        
        x = (torch.randn(D, T) + 1j * torch.randn(D, T)) * 10 ** (SNR/20) * noise.sigma_noise
    
    else:
        
        x = (torch.randn(1, T) + 1j * torch.randn(1, T)) * 10 ** (SNR/20) * noise.sigma_noise
        x = torch.Tensor.repeat(x, (D, 1))
    
    return A @ x + noise.generate(T).T, theta


def generate_data(N, T, D, SNR, array: ArrayModel, noise: CorrelatedNoise, coherent=False):

    """generate a dataset consisting of array signals of size (N, T, M) 
    and their corresponding incident angles (N, D)"""
    
    observations = []
    angles = []
    
    for _ in range(N):
        
        y, phi = generate_signal(T, D, SNR, array, noise, coherent=coherent)
        observations.append(y.T)
        angles.append(phi)
    
    observations = torch.stack(observations, dim=0)
    angles = torch.stack(angles, dim=0)
    
    return observations, angles


def get_spectrum(En, array: ArrayModel, noise: CorrelatedNoise):

    """
    return MUSIC pseudospectrum

    INPUT: 
    En: Noise subspace of size (M, M-D) or (N, M, M-D)
    array: predefined array model 
    noise: predefined noise model  

    OUPUT: 
    spectrum: pseudospectrum of size (nbSamples_spectrum) or (N, nbSamples_spectrum)
    """
    
    if noise is None:
        
        array.array = array.array.to(En.device)
        A = array.get_steering_vector(angles_spectrum.to(En.device)).to(En.device)
        spectrum = 1 / torch.norm(En.conj().transpose(1, 2) @ A, dim=1) ** 2
    
    else:
        
        array.array = array.array.to(En.device)
        L = noise.L.to(En.device)
        A = array.get_steering_vector(angles_spectrum.to(En.device)).to(En.device)
        spectrum = 1 / torch.norm(En.conj().transpose(1, 2) @ L @ A, dim=1) ** 2
    
    return spectrum


def MUSIC(X, D, array: ArrayModel, noise: CorrelatedNoise):

    """
    return estimated angles and pseudospectrum using MUSIC algorithm
    
    INPUT: 
    X: antenna array signal of size (M, T)
    D: number of sources
    array: predefined array model
    noise: predefined noise model

    OUTPUT: 
    spectrum: pseudospectrum of size (nbSamples_spectrum) and estimated incident angle of size (D)
    """
    
    M, T = X.shape
    cov = X @ X.T.conj() / T
    A = array.get_steering_vector(angles_spectrum)

    if noise is None:
        
        vals, vecs = torch.linalg.eigh(cov)
        vecs = vecs[:, vals.sort()[1]]
        E = vecs[:, :(M - D)]
        spectrum = 1 / torch.norm(E.T.conj() @ A, dim=0) ** 2

    else:
        
        U = noise.U
        cov = U @ cov @ U.T.conj()
        vals, vecs = torch.linalg.eigh(cov)
        vecs = vecs[:, vals.sort()[1]]
        E = vecs[:, :(M - D)]
        spectrum = 1 / torch.norm(E.T.conj() @ U @ A, dim=0) ** 2

    idx, _ = find_peaks(spectrum)
    idx = idx[list(torch.argsort(spectrum[idx])[-D:])]
    
    return spectrum, torch.cat((angles_spectrum[idx], torch.pi * (torch.rand(D - len(idx)) - 1/2)))


def RootMUSIC(Rx, D):

    """
    differentiable RootMUSIC algorithm

    INPUT:
    Rx: estimated covariance matrix of received signals
    D: number of directions

    OUTPUT:
    estimated incident angles
    """

    M = Rx.shape[1]
    vals, vecs = torch.linalg.eigh(Rx)
    idx = torch.sort(vals, dim=1)[1].unsqueeze(dim=1).repeat(repeats=(1, M, 1))
    vecs = torch.gather(vecs, dim=2, index=idx)
    En = vecs[:, :, :(M - D)]

    R = En @ En.conj().transpose(-2, -1)
    coef = torch.zeros(Rx.shape[0], 2 * M - 1, dtype=torch.complex64, device=Rx.device)
    for i in range(2 * M - 1):
        coef[:, i] = torch.diagonal(R, offset=i - M + 1, dim1=-2, dim2=-1).sum(dim=-1)
    coef = coef / coef[:, -1].unsqueeze(1)
    C = torch.zeros(Rx.shape[0], 2 * M - 2, 2 * M - 2, dtype=torch.complex64, device=Rx.device)
    C[:, :, -1] = - coef[:, :-1]
    C[:, 1:, :-1] = torch.eye(2 * M - 3, dtype=torch.complex64, device=Rx.device)
    roots = torch.linalg.eigvals(C)

    roots = roots[torch.abs(roots) < 1]
    roots = roots.reshape(-1, M - 1)

    indx_sorted = torch.argsort(torch.abs(torch.abs(roots) - 1), dim=1)
    roots = torch.gather(roots, dim=1, index=indx_sorted)[:, :D]
    
    return - torch.arcsin(torch.angle(roots) / torch.pi)


def ESPRIT(Rx, D):
    
    """
    differentiable ESPRIT function

    INPUT:
    Rx: estimated covariance of received signals
    D: number of directions

    OUTPUT:
    estimated incident angles
    """

    M = Rx.shape[1]
    vals, vecs = torch.linalg.eigh(Rx)
    idx = torch.sort(vals, dim=1)[1].unsqueeze(dim=1).repeat(repeats=(1, M, 1))
    vecs = torch.gather(vecs, dim=2, index=idx)
    Es = vecs[:, :, (M - D):]
    Phi = torch.linalg.pinv(Es[:, :-1, :]) @ Es[:, 1:, :]
    vals = torch.linalg.eigvals(Phi)
    
    return - torch.arcsin(torch.angle(vals) / torch.pi)


def permutations(arr):

    """return all permutation of a list"""
    
    if len(arr) == 1:
        return [arr]
    
    res = []
    
    for i in range(len(arr)):
        res += [[arr[i]] + perm for perm in permutations(arr[:i]+arr[i+1:])]
    
    return res


def RMSPE(pred, true): 

    """
    return root mean square periodic error between predicted value and true value
    
    INPUT:
    pred: predicted angle of size (B, D)
    true: true ange of size (B, D)

    OUTPUT:
    loss: scalar value representing RMSPE 
    """
    
    perm = torch.zeros(true.shape[0], factorial(true.shape[1]), true.shape[1])
    
    for i in range(true.shape[0]):
        perm[i] = torch.Tensor(permutations(list(true[i])))
    
    perm = perm.to(device=dev)
    diff = torch.fmod(perm - pred.unsqueeze(1) + torch.pi / 2, torch.pi) - torch.pi / 2 
    loss = torch.mean(torch.sqrt((diff ** 2).mean(dim=-1)).amin(dim=-1))
    
    return loss


""" functions below are reserved for CNN architecture"""

def multihot_encode(true):

    """
    return a batch of multihot vector using true angles
    
    INPUT:
    true: a batch of ground truth angle of size (B, D)

    OUTPUT:
    loss: a batch of ground truth angles encoded to multihot of size (B, nbSample_cnn)
    """ 

    true_coded = true + torch.pi / 2
    true_coded = torch.floor(true_coded * nbSamples_cnn / torch.pi).to(torch.int64).to(device=dev)
    
    multihot = torch.zeros(true.shape[0], nbSamples_cnn).to(device=dev)
    return multihot.scatter_(1, true_coded, 1)


def multihot_decode(pred, D):

    """
    return a batch of predicted angles using output of cnn
    
    INPUT:
    pred: output of cnn of size (B, nbSamples_cnn)
    D: number of sources

    OUTPUT:
    a batch of predicted angles encoded to multihot of size (B, D)
    """ 

    _, pred_index = torch.topk(input=pred, k=D, dim=1)
    result = (torch.arange(0, nbSamples_cnn, 1) * torch.pi / nbSamples_cnn - torch.pi / 2).unsqueeze(0).repeat(pred.shape[0], 1).to(dev)
    return torch.gather(input=result, dim=1, index=pred_index.to(dev)).to(dev)













def str2bool(v):
    
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')