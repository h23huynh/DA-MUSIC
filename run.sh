#!/bin/bash

MC=100

T=200
M=8
D=3

perturbation=0.1
calibrated=true
coherent=false

mkdir saved_models

python3 generate_result_file.py

python3 run_models.py -MC=$MC -T=$T -M=$M -D=$D -SNR=-10 -perturbation=$perturbation -calibrated=$calibrated -coherent=$coherent -order 0 &
sleep 0.1
python3 run_models.py -MC=$MC -T=$T -M=$M -D=$D -SNR=-5 -perturbation=$perturbation -calibrated=$calibrated -coherent=$coherent -order 1 &
sleep 0.1
python3 run_models.py -MC=$MC -T=$T -M=$M -D=$D -SNR=0 -perturbation=$perturbation -calibrated=$calibrated -coherent=$coherent -order 2 &
sleep 0.1
python3 run_models.py -MC=$MC -T=$T -M=$M -D=$D -SNR=5 -perturbation=$perturbation -calibrated=$calibrated -coherent=$coherent -order 3 &
sleep 0.1
python3 run_models.py -MC=$MC -T=$T -M=$M -D=$D -SNR=10 -perturbation=$perturbation -calibrated=$calibrated -coherent=$coherent -order 4 &
sleep 0.1
python3 run_models.py -MC=$MC -T=$T -M=$M -D=$D -SNR=15 -perturbation=$perturbation -calibrated=$calibrated -coherent=$coherent -order 5 &

wait

python3 plot_results.py

echo Finished

