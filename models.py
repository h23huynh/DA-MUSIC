import torch 
import torch.nn as nn
import torch.nn.functional as F

from utils import *

dev = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class DA_MUSIC(nn.Module): 
    
    def __init__(self, M: int, D: int, array: ArrayModel, device=dev):
        
        super().__init__()

        self.M = M
        self.D = D
        self.array = array
        self.device = device

        self.bn = nn.BatchNorm1d(2*self.M, device=device)
        self.rnn = nn.GRU(input_size=2*self.M, hidden_size=2*self.M, num_layers=1, batch_first=True, device=device)
        self.fc = nn.Linear(in_features=2*self.M, out_features=2*self.M*self.M, device=device)
        self.mlp = nn.Sequential(nn.Linear(in_features=nbSamples_spectrum, out_features=2*self.M, device=device), nn.ReLU(),
                                 nn.Linear(in_features=2*self.M, out_features=2*self.M, device=device), nn.ReLU(),
                                 nn.Linear(in_features=2*self.M, out_features=2*self.M, device=device), nn.ReLU(),
                                 nn.Linear(in_features=2*self.M, out_features=D, device=device))

    def forward(self, x):
        
        x = torch.cat((torch.real(x), torch.imag(x)), dim=-1)
        x = self.bn(x.transpose(1, 2)).transpose(1, 2)
        _, x = self.rnn(x)
        x = self.fc(x[-1])
        x = x.reshape(-1, 2, self.M, self.M)
        x = x[:, 0, :, :] + 1j * x[:, 1, :, :]
        vals, vecs = torch.linalg.eig(x)
        idx = torch.sort(torch.abs(vals), dim=1)[1].unsqueeze(dim=1).repeat(repeats=(1, self.M, 1))
        vecs = torch.gather(vecs, dim=2, index=idx)
        E = vecs[:, :, :(self.M - self.D)]
        y = get_spectrum(E, array=self.array, noise=None)
        y = self.mlp(y)
        
        return y
    


class DA_MUSIC_v2(nn.Module): 
    
    def __init__(self, M: int, D: int, array: ArrayModel, noise: CorrelatedNoise, device=dev):
        
        super().__init__()

        self.M = M
        self.D = D
        self.array = array
        self.noise = noise
        self.device = device

        self.bn = nn.BatchNorm1d(2*self.M, device=device)
        self.rnn = nn.GRU(input_size=2*self.M, hidden_size=2*self.M, num_layers=1, batch_first=True, device=device)
        self.fc = nn.Linear(in_features=2*self.M, out_features=2*self.M*self.M, device=device)
        self.mlp = nn.Sequential(nn.Linear(in_features=nbSamples_spectrum, out_features=2*self.M, device=device), nn.ReLU(),
                                 nn.Linear(in_features=2*self.M, out_features=2*self.M, device=device), nn.ReLU(),
                                 nn.Linear(in_features=2*self.M, out_features=2*self.M, device=device), nn.ReLU(),
                                 nn.Linear(in_features=2*self.M, out_features=D, device=device))

    def forward(self, x):
        
        x_ = torch.cat((torch.real(x), torch.imag(x)), dim=-1)
        x_ = self.bn(x_.transpose(1, 2)).transpose(1, 2)
        _, x_ = self.rnn(x_)
        x_ = self.fc(x_[-1])
        x_ = x_.reshape(-1, 2, self.M, self.M)
        x_ = torch.complex(x_[:, 0, :, :], x_[:, 1, :, :])
        
        Rx = x_ @ x_.conj().transpose(1, 2)

        vals, vecs = torch.linalg.eigh(Rx)
        idx = torch.sort(torch.abs(vals), dim=1)[1].unsqueeze(dim=1).repeat(repeats=(1, self.M, 1))
        vecs = torch.gather(vecs, dim=2, index=idx)
        E = vecs[:, :, :(self.M - self.D)]
        y = get_spectrum(E, array=self.array, noise=None)
        y = self.mlp(y)
        
        return y
    


class RNN(nn.Module): 
    
    def __init__(self, M: int, D: int, device=dev):
        
        super().__init__()

        self.M = M
        self.D = D
        self.device = device

        self.bn = nn.BatchNorm1d(2*M, device=device)
        self.rnn = nn.GRU(input_size=2*M, hidden_size=2*M, num_layers=1, batch_first=True, device=device)

        self.mlp = nn.Sequential(nn.Linear(in_features=2*M, out_features=2*M*M, device=device), nn.ReLU(),
                                 nn.Linear(in_features=2*M*M, out_features=2*M*M, device=device), nn.ReLU(),
                                 nn.Linear(in_features=2*M*M, out_features=D, device=device))

    def forward(self, x):
        
        x = torch.cat((torch.real(x), torch.imag(x)), dim=-1)
        x = self.bn(x.transpose(1, 2)).transpose(1, 2)
        _, x = self.rnn(x)
        y = self.mlp(x[-1])

        return y
    


class CNN(nn.Module):

    def __init__(self, M: int, D:int, device=dev):

        super().__init__()
        self.M = M
        self.D = D
        self.net = nn.Sequential(nn.Conv2d(in_channels=3, out_channels=256, kernel_size=(3, 3), stride=2, device=device),
                                 nn.BatchNorm2d(256, device=device), nn.ReLU(),
                                #  nn.Conv2d(in_channels=256, out_channels=256, kernel_size=(2, 2), device=device),
                                #  nn.BatchNorm2d(256, device=device), nn.ReLU(),
                                 nn.Conv2d(in_channels=256, out_channels=256, kernel_size=(2, 2), device=device),
                                 nn.BatchNorm2d(256, device=device), nn.ReLU(),
                                 nn.Conv2d(in_channels=256, out_channels=256, kernel_size=(2, 2), device=device),
                                 nn.BatchNorm2d(256, device=device), nn.ReLU(), nn.Flatten(),
                                 nn.Linear(in_features=256*(int((self.M - 3)/2)-1), out_features=4096, device=device),
                                 nn.ReLU(), nn.Dropout(0.3),
                                 nn.Linear(in_features=4096, out_features=2048, device=device),
                                 nn.ReLU(), nn.Dropout(0.3),
                                 nn.Linear(in_features=2048, out_features=1024, device=device),
                                 nn.ReLU(), nn.Dropout(0.3),
                                 nn.Linear(in_features=1024, out_features=nbSamples_cnn, device=device),
                                 nn.Sigmoid())
        self.device = device
        

    def forward(self, x):

        Rx = x.transpose(1, 2) @ x.conj() / x.shape[1]

        X = torch.stack((torch.real(Rx),
                         torch.imag(Rx),
                         torch.angle(Rx)), dim=1).to(self.device)
        
        return self.net(X)