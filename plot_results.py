import numpy as np
import matplotlib.pyplot as plt 
plt.rcParams['figure.dpi']=150
import re

file = open('results.txt', 'r')
    
list = [re.findall("\d+\.\d+", x) for x in file.readlines()]
list = [float(x[0]) for x in list if x != []]
A = np.array(list).reshape(6, 5).T

snr = [-10, -5, 0, 5, 10, 15]
plt.plot(snr, A[0], color='cyan', label='MUSIC', marker='.')
plt.plot(snr, A[1], color='red', label='DA-MUSIC', marker='.')
plt.plot(snr, A[2], color='green', label='Proposed Modified DA-MUSIC', marker='.')
plt.plot(snr, A[3], color='blue', label='Proposed DD Model', marker='.')
plt.plot(snr, A[4], color='orange', label='CNN', marker='.')
plt.legend(loc='upper right')
plt.xlabel('SNR')
plt.ylabel('RMSE(rad)')
plt.grid(True)
plt.savefig('results.png')